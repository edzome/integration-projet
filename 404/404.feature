Feature: Display the 404 error page

Scenario: Any user clink on an orphan link.
Given: i am a guest on your website
When: i click on any link
And: the link is not related to any html page
Then: the site should display a beautiful 404 page